<?php

/*
 * This file demonstrates extending the annotations class for a resource.
 * This all works exactly the same for labels.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Class extending the base annotations class
class IngressAnnotations extends \Nucleardog\K8s\Resources\Attributes\Annotations
{
	public function entrypoints(): ?array
	{
		if (!isset($this['traefik.ingress.kubernetes.io/router.entrypoints']))
			return null;
		return explode(',', $this['traefik.ingress.kubernetes.io/router.entrypoints']);
	}
}

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

// Override the annotations for the Ingress kind
Nucleardog\K8s\Kind\Ingress::setAnnotationsAttribute(IngressAnnotations::class);

// Now fetch an ingress
$ingress = $k8s->ingresses->findOrFail('system-dashboard');

$traefikEntrypoints = $ingress->annotations->entrypoints;

writeln(sprintf('Ingress %s has traefik entrypoints:', $ingress->name));
if (empty($traefikEntrypoints))
{
	writeln(' - None found!');
}
else
{
	foreach ($traefikEntrypoints as $entrypoint)
	{
		writeln(sprintf(' - %s', $entrypoint));
	}
}
writeln();
