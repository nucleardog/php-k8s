<?php

/*
 * This file demonstrates fetching a list of pods related to a deployment.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

// Fetch the 'system-dashboard' ingress from the default namespace
//$ingress = $k8s->ingresses->findOrFail('system-dashboard');
$ingress = $k8s->ingresses->findOrFail('marketplace-api');


writeln(sprintf('Ingress: %s', $ingress->name));
writeln();

writetabular([36], "Service Name", "Cluster IP");
foreach ($ingress->services as $service)
{
	writetabular([36], $service->name, $service->ip);

}