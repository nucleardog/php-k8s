<?php

/*
 * This file demonstrates fetching a list of pods related to a deployment.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

// Fetch the 'traefik' deployment from 'kube-system'
$traefikDeployment = $k8s->namespace('kube-system')->deployments->findOrFail('traefik');

$pods = $traefikDeployment->pods;

writetabular([32, 15, 7, 5], "Name", "Pod IP", "Running", "Ready", "Containers");
foreach ($pods as $pod)
{
	$containers = $pod['spec']['containers']->pluck('name')->all();
	writetabular([32, 15, 7, 5],
		$pod->name,
		$pod->ip,
		$pod->running ? 'yes' : 'no',
		$pod->ready ? 'yes' : 'no',
		implode(', ', $containers)
	);
}