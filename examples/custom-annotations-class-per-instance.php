<?php

/*
 * This file demonstrates extending the annotations class for a resource, but
 * using different classes fore each instance of the k8s library.
 * This all works exactly the same for labels.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Mock separate annotation classes
class AnnotationsA extends Nucleardog\K8s\Resources\Attributes\Annotations { public function implementation() { return 'A'; } }
class AnnotationsB extends Nucleardog\K8s\Resources\Attributes\Annotations { public function implementation() { return 'B'; } }
class AnnotationsC extends Nucleardog\K8s\Resources\Attributes\Annotations { public function implementation() { return 'C'; } }

// Instantiate the client
$client = createKubernetesClient();

// Instantiate three copies of the library
$k8sA = createLibrary($client);
$k8sB = createLibrary($client);
$k8sC = createLibrary($client);

// Override the annotations for deployments.
// We apply:
// - AnnotationsA to library instance A
// - AnnotationsB to library instance B
// - AnnotationsC as the default
Nucleardog\K8s\Kind\Deployment::setAnnotationsAttribute(AnnotationsA::class, $k8sA);
Nucleardog\K8s\Kind\Deployment::setAnnotationsAttribute(AnnotationsB::class, $k8sB);
Nucleardog\K8s\Kind\Deployment::setAnnotationsAttribute(AnnotationsC::class);

// Fetch the same deployment via each
$deploymentA = $k8sA->namespace('kube-system')->deployments->findOrFail('traefik');
$deploymentB = $k8sB->namespace('kube-system')->deployments->findOrFail('traefik');
$deploymentC = $k8sC->namespace('kube-system')->deployments->findOrFail('traefik');

// Now display their annotation information
writetabular([30], 'Instance', 'A');
writetabular([30], 'Annotations Class', get_class($deploymentA->annotations));
writetabular([30], 'Implementation Property', $deploymentA->annotations->implementation);

writeln();

writetabular([30], 'Instance', 'B');
writetabular([30], 'Annotations Class', get_class($deploymentB->annotations));
writetabular([30], 'Implementation Property', $deploymentB->annotations->implementation);

writeln();

writetabular([30], 'Instance', 'C');
writetabular([30], 'Annotations Class', get_class($deploymentC->annotations));
writetabular([30], 'Implementation Property', $deploymentC->annotations->implementation);