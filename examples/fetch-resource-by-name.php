<?php

/*
 * This file demonstrates fetching a single resource by name.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

// Fetch the "kube-dns" service from the kube-system namespace
$kubeDnsService = $k8s->namespace('kube-system')->services->find('kube-dns');

// `find` can return null if the resource is not found. We should check for that.
if (!isset($kubeDnsService))
{
	writeln('kube-dns service not found');
}
else
{
	writetabular([16], 'Name', $kubeDnsService->name);
	writetabular([16], 'Cluster IP', $kubeDnsService->ip);

	writetabular([16], "");
	writetabular([36], 'Label', 'Value');
	$kubeDnsService->labels->each(fn(string $value, string $label) => writetabular([36], $label, $value));

	writetabular([16], "");
	writetabular([36], 'Annotation', 'Value');
	$kubeDnsService->annotations->each(fn(string $value, string $label) => writetabular([36], $label, $value));
}


// You can also use the `findOrFail` method to trigger an exception if the
// resource cannot be found.

writeln();
write('Looking for `kubernetes` service in `default` namespace... ');
try
{
	$kubernetesService = $k8s->services->findOrFail('kubernetes');
	writeln('Found!');
}
catch (NuclearDog\K8s\Exceptions\ResourceNotFoundException)
{
	writeln('Not Found!');
}

write('Looking for `does-not-exist` service in `kube-system` namespace... ');
try
{
	$doesNotExistService = $k8s->namespace('kube-system')->services->findOrFail('does-not-exist');
	writeln('Found!');
}
catch (NuclearDog\K8s\Exceptions\ResourceNotFoundException)
{
	writeln('Not Found!');
}

