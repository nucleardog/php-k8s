<?php

/*
 * This file demonstrates listing a resource which is not implemented in the
 * library.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

// Fetch a Collection of all service accounts in the default namespace
$default_service_accounts = $k8s->serviceaccounts->all();

// Fetch a Collection of all service accounts in the kube-system namespace
$system_service_accounts = $k8s->namespace('kube-system')->serviceaccounts->all();


// Print the lists
writeln('default:');
writetabular([7, 36], "", "Service Account", "Image Pull Secrets");
// There is no custom resource class implemented for this Kind (ServiceAccount),
// so a generic Resource class will be returned. For other resources that
// have implementations (e.g., Service), that would be returned instead.
//
// All of the custom Kind classes extend from this base Resource class, so
// you can safely type hint the Resource class for currently-unimplemented
// resources and any future implementation of those resources will remain
// backward compatible.
$default_service_accounts->each(function(Nucleardog\K8s\Resources\Resource $service_account) {
	// Values on the resource can be accessed as object properties, in which
	// case the class can/will provide additional functionality or wrappers,
	// or synthesize entirely new properties or automatically fetch
	// relationships (e.g., the pods related to a service).

	$serviceAccountName = $service_account->name;

	// You can still access values with no special behaviour via the
	// object property syntax.
	$serviceAccountImagePullSecrets = array_map(fn($imagePullSecret) => $imagePullSecret['name'], $service_account->imagePullSecrets ?? []);

	writetabular([7, 36], "", $serviceAccountName, implode(', ', $serviceAccountImagePullSecrets));
});

writeln('kube-system:');
writetabular([7, 36], "", "Service Account", "Image Pull Secrets");
$system_service_accounts->each(function(Nucleardog\K8s\Resources\Resource $service_account) {
	// Alternatively, you can access the raw data directly using
	// array syntax.

	$serviceAccountName = $service_account['metadata']['name'];
	$serviceAccountImagePullSecrets = array_map(fn($imagePullSecret) => $imagePullSecret['name'], $service_account['imagePullSecrets'] ?? []);

	writetabular([7, 36], "", $serviceAccountName, implode(', ', $serviceAccountImagePullSecrets));
});

writeln();