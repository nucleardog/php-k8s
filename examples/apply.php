<?php

/*
 * This file demonstrates applying a JSON/YAML file to the cluster.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);


////////////////////////////////////////////////////////////////////////////////
// Create/Update Resources From a File
// This is roughly equivalent to `kubectl apply`.

// First, read the resources from your JSON/YAML file
$resources = $k8s->json()->fromFile(__DIR__.'/apply.json');
//$resources = $k8s->yaml()->fromFile(__DIR__.'/apply.yaml');

// This returns a collection of resources. You can then apply them:
$resources->each(fn($resource) => $resource->apply());

// The resources are automatically updated with the data returned during
// application. Data which was not available in the original document
// is now available:
writetabular([16, 32, 40], 'Namespace', 'Name', 'UID');
$resources->each(function($resource) {
	writetabular([16, 32, 40], $resource->namespace, $resource->name, $resource->uid);
});

////////////////////////////////////////////////////////////////////////////////
// Delete Resources From a File
// This is like an "undo" for the earlier apply.

// You can also use this to perform a "delete" of resources
$resources = $k8s->json()->fromFile(__DIR__.'/apply.json');
//$resources = $k8s->yaml()->fromFile(__DIR__.'/apply.yaml');

$resources->each(function($resource) {
	$resource->delete();
});