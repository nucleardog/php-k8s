<?php

/*
 * This file demonstrates watching k8s resources. It will not exit on its own,
 * but will watch and print events until you terminate it (e.g., Ctrl+C).
 */

declare(strict_types=1);

use Nucleardog\K8s\Watcher\EventType;
use Nucleardog\K8s\Kind\Pod;

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

$pods = $k8s->global()->pods;

$pods->on(EventType::ADDED)->call(
	function(EventType $event, Pod $pod)
	{
		echo sprintf('Pod Added: Namespace=%s Name=%s', $pod->namespace, $pod->name).PHP_EOL;
	}
);

$pods->on(EventType::MODIFIED)->call(
	function(EventType $event, Pod $pod)
	{
		echo sprintf('Pod Modified: Namespace=%s Name=%s', $pod->namespace, $pod->name).PHP_EOL;
	}
);

$pods->on(EventType::DELETED)->call(
	function(EventType $event, Pod $pod)
	{
		echo sprintf('Pod Deleted: Namespace=%s Name=%s', $pod->namespace, $pod->name).PHP_EOL;
	}
);

echo "Starting listener...".PHP_EOL;
$k8s->watcher()->start()->listen();