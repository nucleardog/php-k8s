<?php

/*
 * This file demonstrates dumping configmaps
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

$configMaps = $k8s->global()->configmaps->all();

foreach ($configMaps as $configMap)
{
	writeln(' * ', $configMap->name);
}