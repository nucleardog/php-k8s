<?php

declare(strict_types=1);

/*
 * This file includes some repeated code from the examples to help simplify
 * and clarify the example code.
 */

/**
 * Instantiate the underlying Kubernetes client
 *
 * This client provides a low level interface to the Kubernetes API, handling
 * basic response encoding, request decoding, and authentication.
 *
 * @return KubernetesClient\Client
 */
function createKubernetesClient(): KubernetesClient\Client
{
	// Use the default configuration loader.
	// This will try, in order:
	//	- A file referenced by the `KUBECONFIG` environment variable.
	//	- Your ~/.kube/config file.
	//	- In-cluster configuration using k8s's injected token/cert
	//	  at /var/run/secrets
	$config = KubernetesClient\Config::LoadFromDefault();

	// Instantiate and return a client using the loaded configuration
	return new KubernetesClient\Client($config);
}


/**
 * Instantiate the nucleardog/k8s lib
 *
 * @param KubernetesClient\Client $client
 * @param array<string,mixed> $options array of OPTION_ consts => values
 * @return Nucleardog\K8s\Kubernetes
 */
function createLibrary(KubernetesClient\Client $client, ?Nucleardog\K8s\Options $options = null): Nucleardog\K8s\Kubernetes
{
	return new Nucleardog\K8s\Kubernetes($client, $options);
}

/**
 * Print text
 *
 * @param ...string $strings one or more strings to be printed space-separated
 * @return void
 */
function write(...$strings): void
{
	echo implode(' ', $strings);
}

/**
 * Print a line of text
 *
 * @param ...string $strings one or more strings to be printed space-separated, terminated by a newline
 * @return void
 */
function writeln(...$strings): void
{
	echo implode(' ', $strings).PHP_EOL;
}

/**
 * Print tabular data
 *
 * Prints each string value passed padded or truncated to the width specified
 * in the lengths array. If there are more string values than lengths, the
 * remaining values will be printed with no padding or truncating, except in
 * the case where the overall line length would exceed 80 characters, in which
 * case the value will be truncated.
 *
 * @param int[] $lengths array of column widths
 * @param ...string $strings column values
 * @return void
 */
function writetabular(array $lengths, ...$strings): void
{
	$parts = [];
	$sum = 0;
	for ($i=0; $i<sizeof($strings); $i++)
	{
		$len = $lengths[$i] ?? null;
		$str = $strings[$i];

		if (empty($len))
		{
			$remaining = 80 - $sum;
			if (strlen($str) > $remaining && $remaining > 3)
			{
				$parts[] = substr($str, 0, $remaining-3).'...';
				$sum += $remaining + 1;
			}
			else if (strlen($str) > $remaining)
			{
				$parts[] = substr($str, 0, $remaining);
				$sum += $remaining + 1;
			}
			else
			{
				$parts[] = $str;
				$sum += strlen($str);
			}
		}
		else
		{
			$sum += $len + 1;
			if (strlen($str) > $len && $len > 3)
				$parts[] = substr($str, 0, $len-3).'...';
			else if (strlen($str) > $len)
				$parts[] = substr($str, 0, $len);
			else
				$parts[] = str_pad($str, $len, ' ', STR_PAD_RIGHT);
		}
	}
	writeln(...$parts);
}