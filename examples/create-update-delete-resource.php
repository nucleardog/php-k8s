<?php

/*
 * This file demonstrates creating a resource, updating it, and deleting it.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);


////////////////////////////////////////////////////////////////////////////////
// Create Resource

// Create a new resource instance
// You can pass values at creation time
$service = $k8s->services->create([
	'metadata' => [
		'name' => 'example-project',
		'labels' => [
			'app.kubernetes.io/name' => 'example-project',
		],
	],
	'spec' => [
		'ports' => [
			[
				'name' => 'http',
				'port' => 80,
				'protocol' => 'TCP',
				'targetPort' => 80,
			]
		],
		'selector' => [
			'app.kubernetes.io/name' => 'example-project',
		],
	],
]);

$service->save();

writeln('Created service:', $service->uid);


////////////////////////////////////////////////////////////////////////////////
// Update Resource

// Fetch the resource
$service = $k8s->services->findOrFail('example-project');

// Change some stuff
$service['spec']['selector']['app.kubernetes.io/part-of'] = 'examples';
$service['spec']['ports'][1] = [
	'name' => 'https',
	'port' => 443,
	'protocol' => 'TCP',
];
$service['spec']['ports'][1]['targetPort'] = 443;
unset($service['spec']['ports'][0]);

// Then save it
$service->save();


$service = $k8s->services->findOrFail('example-project');
writeln('Patched Service:');
writeln(json_encode($service->toArray(), \JSON_PRETTY_PRINT));


////////////////////////////////////////////////////////////////////////////////
// Delete Resource

// Fetch the resource
$service = $k8s->services->findOrFail('example-project');

// Delete it
$service->delete();

writeln('Deleted service.');