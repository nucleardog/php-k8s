<?php

/*
 * This file demonstrates listing a resource which has a custom Kind implemented
 * in the library.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

// Fetch a Collection of all services in the default namespace
$default_services = $k8s->services->all();

// Fetch a Collection of all services in the kube-system namespace
$system_services = $k8s->namespace('kube-system')->services->all();

// Print a list of default services and their IPs

writeln('default:');
writetabular([7, 36], "", "Service Name", "Cluster IP");
// There is a custom resource class implemented for this Kind (Service), so
// that is what will be returned. For any resource that does not have a
// custom implementation, an instance of the generic Resource class will be
// returned instead.
$default_services->each(function(Nucleardog\K8s\Kind\Service $service) {
	// Values on the resource can be accessed as object properties, in which
	// case the class can/will provide additional functionality or wrappers,
	// or synthesize entirely new properties or automatically fetch
	// relationships (e.g., the pods related to a service).

	$serviceName = $service->name;
	$serviceIp = $service->ip;

	writetabular([7, 36], "", $serviceName, $serviceIp);
});

writeln('kube-system:');
writetabular([7, 36], "", "Service Name", "Cluster IP");
$system_services->each(function(Nucleardog\K8s\Kind\Service $service) {
	// Alternatively, you can access the raw data directly using
	// array syntax.

	$serviceName = $service['metadata']['name'];
	$serviceIp = $service['spec']['clusterIP'];

	writetabular([7, 36], "", $serviceName, $serviceIp);
});

writeln();