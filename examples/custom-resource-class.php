<?php

/*
 * This file demonstrates implementing your own resource class.
 */

declare(strict_types=1);

// Require the common helper functions and composer autoload
require_once(__DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', 'vendor', 'autoload.php']));
require_once(__DIR__.DIRECTORY_SEPARATOR.'common.php');

// Create a class extending the base Resource class to represent our custom
// resource.
class ServiceAccount extends \Nucleardog\K8s\Resources\Resource
{
	// You can define additional methods which will be exposed as properties
	// via object syntax to extend the functionality.
	//
	// If these have the same name as one of the existing fields, the method
	// will take priority and override directly accessing the values.
	//
	// E.g., this can be called with `$resource->imagePullSecrets`.
	//
	// NOTE: These methods are _not_ called when using array syntax. Accessing
	//       $resource['imagePullSecrets'] will return the raw data from the
	//       response.

	public function imagePullSecrets(): array
	{
		return $this['imagePullSecrets']->pluck('name')->all();
	}
}

// Instantiate the client and library
$client = createKubernetesClient();
$k8s = createLibrary($client);

// Register our class for the appropriate Kind
$k8s->registerKind('ServiceAccount', ServiceAccount::class);

// Now fetch the resource
$defaultServiceAccount = $k8s->serviceaccounts->findOrFail('default');

writetabular([36], "Service Account", "Image Pull Secrets");
writetabular([36], $defaultServiceAccount->name, implode(', ', $defaultServiceAccount->imagePullSecrets));