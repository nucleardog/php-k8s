# Changelog

## Unreleased

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.10...master)

## v0.0.10 (2024-10-13)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.9...v0.0.10)

* Fix dependency on `travisghansen/kubernetes-client-php` (last release depended
  on master; pinned released version now).

## v0.0.9 (2024-10-13)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.8...v0.0.9)

* Can now work on all namespaces when dealing with namespaced resources with
  `->global()` instead of `->namespace()`.
* Moved to Data library that keeps distinction between arrays/objects.
  all namespaces.
* Broken: Saving/patching not updated.

## v0.0.9 (2024-10-13)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.8...v0.0.9)

* Can now work on all namespaces when dealing with namespaced resources with
  `->global()` instead of `->namespace()`.

## v0.0.8 (2024-04-22)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.7...v0.0.8)

* Fix: Requests for non-existent resources now throw a `ResourceNotFoundException`
       instead of an exception about incorrect Kind.
* Fix: Resource::delete() no longer expects the resource returned in response.

## v0.0.7 (2024-04-02)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.6...v0.0.7)

* Response::throws() now throws appropriately typed exception when a resource is
  not found.
* Resource::delete() ignores ResourceNotFoundException.

## v0.0.6 (2024-04-02)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.5...v0.0.6)

* Add server-side apply. (See [examples/apply.php](examples/apply.php).)
* Resource class now supports modification of data through the array access interface.
  Tracks changes as a JSON Patch to allow changes to be saved later.
* Added methods to Resource to allow saving, applying, deleting resources directly.
  (See [examples/create-update-delete-resource.php](examples/create-update-delete-resource.php).

## v0.0.5 (2024-03-25)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.4...v0.0.5)

* Update for compatibility with Laravel 11.

## v0.0.4 (2024-02-14)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.3...v0.0.4)

* Allows overriding the collection class for Annotations and Labels to allow
  easily defining custom helpers for your particular annotation/label schemes.
* Simplified internal interfaces by tracking the namespace directly on the
  NamespacedResourceType, making usage within the code identical and removing
  the need to add separate "namespace" parameters all over the place.
* Added basic watch functionality for listening to resource events.

## v0.0.3 (2024-02-08)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.2...v0.0.3)

* Split up responsibilities of main Kubernetes class and generally
  rearchitected to clean code up.


## v0.0.2 (2024-02-07)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/compare/v0.0.1...v0.0.2)

* Added more resource types:
  * ConfigMap
  * Deployment
  * Ingress
  * Pod
  * Service
* Various cleanup and bugfixes.


## v0.0.1 (2024-02-07)

[View Commits](https://gitlab.com/nucleardog/php-k8s/-/commits/v0.0.1)

* Initial WIP commit.