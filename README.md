# k8s

A work-in-progress library for developing against the Kubernetes API from PHP
with higher level abstractions and richer resources.

# Usage

## Instantiation

The library depends heavily on the underlying `kubernetes-client-php` library. Instantiate that
then instantiate this library as a wrapper around it:

```php
<?php
// Instantiate underlying client
$config = KubernetesClient\Config::LoadFromDefault();
$client = new KubernetesClient\Client($config);

// Instantiate this library
$k8s = new Nucleardog\K8s\Kubernetes($client);
```

## Reading Resources

Types are typically identified by their plural name as registered in Kubernetes (e.g., if you
would use `kubectl get services` -> `services`). Resources are initially filtered by this, then
you may further filter the resources from there.

```php
<?php

// Fetch all services
$services = $k8s->services->all();

// Fetch a single service by name
$service = $k8s->services->find('my-service');

// Fetch a single service by name, throwing an exception if not found
$service = $k8s->services->findOrFail('my-service');

// Fetch a service by its labels
$service = $k8s->services->whereLabels(new Collection([
	'app.kubernetes.io/name' => 'my-service',
]));
```

## Creating Resources

Any resource can be created by passing the required fields to `create()`. The resource is not
actually submitted to kubernetes until you call save:

```php
<?php

// Data can be specified during creation
$my_service = $k8s->services->create([
	'metadata' => [
		'name' => 'my-service',
		'labels' => [
			'app.kubernetes.io/name' => 'example-project',
		],
	],
	'spec' => [
		'ports' => [],
		'selector' => [
			'app.kubernetes.io/name' => 'example-project',
		],
	],
]);

// Or by setting fields on the object afterwards
$my_service['spec']['ports'][0] = [
	'name' => 'http',
	'port' => 80,
	'protocol' => 'TCP',
	'targetPort' => 80,
];

// You must save the resource to actually have these changes reflected anywhere.
$my_service->save();
```

## Modifying Resources

To update values on a resource, simply load it, make the modifications, then call save:

```php
<?php

// Load the resource
$service = $k8s->services->findOrFail('my-service');

// Change some stuff
$service['spec']['selector']['app.kubernetes.io/part-of'] = 'examples';

// Save it
$service->save();
```

You may also delete resources in a similar way:

```php
<?php

// Load the resource
$service = $k8s->services->findOrFail('my-service');

// Remove it
$service->delete();
```

## Watching Resources

Watches can be registered on any resource type. The library will handle decoding te event
into a resource instance for you:

```php
<?php

$k8s->services->on(Nucleardog\K8s\Watcher\EventType::ADDED)->call(
	function(Nucleardog\K8s\EventType $event, Nucleardog\K8s\Kind\Pod $pod) {
		echo sprintf('Pod Added: Namespace=%s Name=%s', $pod->namespace, $pod->name).PHP_EOL;
	},
);

$k8s->watcher->start()->listen();
```

If a resource class does not exist, you will instead get an instance of `Nucleardog\K8s\Resources\Resource`.

## Namespaces

All calls can be preceeded by a call to `namespace()` to override the default namespace.

```php
<?php

$service = $k8s->namespace('kube-system')->services->findOrFail('traefik');

```

You may also override the namespace to use when none is specified by setting it in the options
during library instantiation:

```php
<?php

// Instantiate underlying client
$config = KubernetesClient\Config::LoadFromDefault();
$client = new KubernetesClient\Client($config);

// Instantiate optiosn

$options = new Nucleardog\K8s\KubernetesOptions(
	namespace: 'my-namespace',
);

// Instantiate this library
$k8s = new Nucleardog\K8s\Kubernetes($client, $options);
```

## Applying Files

This library can load resource definitions from JSON and YAML files.

```php
<?php

$resources = $k8s->json()->fromFile('my-file.json');
//$resources = $k8s->yaml()->fromFile('my-file.yaml');
// These also support fromStream and fromString

```

Once loaded, you can "apply" the resources:

```php
<?php

$resources->each(fn($resource) => $resource->apply());
```

Or "delete" the rsources:

```php
<?php

$resources->each(fn($resource) => $resource->delete());
```

# Examples

See [the examples](examples/) for additional information.

# Thanks

* Travis Glenn Hansen for [kubernetes-client-php](https://github.com/travisghansen/kubernetes-client-php)
  which is the "no nonsense PHP client" handling all of the low level
  authentication and communication concerns so we can add all this nonsense
  on top.

# Legal

Copyright 2024 Adam Pippin <hello@adampippin.ca>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this project except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.