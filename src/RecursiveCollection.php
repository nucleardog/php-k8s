<?php

declare(strict_types=1);

namespace Nucleardog\K8s;
use Illuminate\Support\Collection;

class RecursiveCollection extends Collection
{
	private array $path = [];
	private \Closure $setCallback;
	private \Closure $unsetCallback;

	public function setPath(array $path): void
	{
		$this->path = $path;
	}

	public function setSetCallback(\Closure $callback): void
	{
		$this->setCallback = $callback;
	}

	public function setUnsetCallback(\Closure $callback): void
	{
		$this->unsetCallback = $callback;
	}

	public function offsetGet($key): mixed
	{
		$value = parent::offsetGet($key);

		if (is_array($value))
		{
			$collection = new static($value);
			$collection->setPath(array_merge($this->path, [$key]));
			if (!empty($this->setCallback))
				$collection->setSetCallback($this->setCallback);
			if (!empty($this->unsetCallback))
				$collection->setUnsetCallback($this->unsetCallback);
			return $collection;
		}
		else
		{
			return $value;
		}
	}

	public function offsetSet($key, $value): void
	{
		$oldValue = isset($this[$key]) ? $this[$key] : null;

		parent::offsetSet($key, $value);

		if (!empty($this->setCallback))
		{
			$this->setCallback->__invoke(array_merge($this->path, [$key]), $value, $oldValue);
		}
	}

	public function offsetUnset($key): void
	{
		parent::offsetUnset($key);

		if (!empty($this->unsetCallback))
		{
			$this->unsetCallback->__invoke(array_merge($this->path, [$key]));
		}
	}
}