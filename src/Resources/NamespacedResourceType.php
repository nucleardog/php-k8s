<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Resources;

class NamespacedResourceType extends ResourceType
{
	private ?string $namespace = null;

	public function getCollectionUrl(): string
	{
		if (isset($this->namespace)) {
			return sprintf(
				'%s/namespaces/%s/%s',
				$this->api->getBaseUrl(),
				$this->namespace ?? 'default',
				$this->plural,
			);
		} else {
			return parent::getCollectionUrl();
		}
	}

	public function getResourceUrl(string $name): string
	{
		if (isset($this->namespace)) {
			return sprintf(
				'%s/namespaces/%s/%s/%s',
				$this->api->getBaseUrl(),
				$this->namespace ?? 'default',
				$this->plural,
				$name,
			);
		} else {
			return parent::getResourceUrl($name);
		}
	}

	public function namespace(string $namespace): NamespacedResourceType
	{
		$resourceType = clone $this;
		$resourceType->namespace = $namespace;
		return $resourceType;
	}

}