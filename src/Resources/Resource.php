<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Resources;
use Nucleardog\K8s\Kubernetes;
use Nucleardog\K8s\Exceptions\ResourceNotFoundException;
use Nucleardog\K8s\Data\Data;
use Nucleardog\K8s\Data\Immutable;

class Resource implements \ArrayAccess, \IteratorAggregate
{
	private static $labels = [];
	private static $annotations = [];
	private array $patches = [];
	private Data $data;

	public function __construct(
		protected readonly Kubernetes $k8s,
		Data $data,
		protected readonly ResourceType $type,
	) {
		$this->setData($data);
	}

	private function setData(Data|array $data): void
	{
		if (is_array($data)) {
			$this->data = new Data($data);
		} else if ($data instanceof Immutable) {
			$this->data = $data->cloneMutable();
		} else {
			$this->data = $data->clone();
		}
	}

	public function __clone(): void
	{
		$this->setData($this->data);
	}

	public function apply(): static
	{
		// TODO: Broken
		$response = $this->k8s->client()->apply(
			$this->getUrl(),
			$this->data,
			['fieldManager' => 'nd-k8s', 'fieldValidation' => 'Strict', 'force' => false],
		)->assertKind($this->type->kind);
		$this->data = $response->toArray();
		return $this;
	}

	public function save(): static
	{
		// TODO: Broken
		if ($this->isSaved())
		{
			$response = $this->k8s->client()->jsonPatch(
				url: $this->getUrl(),
				body: $this->patches,
			);
		}
		else
		{
			$response = $this->k8s->client()->put(
				url: $this->getUrl(),
				body: $this->data,
			);
		}
		$response->throws()->assertKind($this->type->kind);
		$this->data = $response->toArray();
		$this->patches = [];
		return $this;
	}

	public function refresh(): static
	{
		$response = $this->k8s->client()->get($this->getUrl())->throws()->assertKind($this->type->kind);
		$this->setData($response);
		return $this;
	}

	public function delete(): static
	{
		try
		{
			$this->k8s->client()->delete($this->getUrl())->throws();
		}
		catch (ResourceNotFoundException)
		{
		}
		return $this;
	}

	private function getUrl(): string
	{
		$type = $this->type;
		if ($type instanceof NamespacedResourceType &&
			!empty($this->data['metadata']['namespace']))
		{
			$type = $type->namespace($this->data['metadata']['namespace']);
		}
		return $type->getResourceUrl($this->name());
	}

	private function formatJsonPatchPath(array $path): string
	{
		// TODO: This should be broken out into a different class for dealing with
		//       patches.
		$path = array_map(function($val) {
			$val = (string)$val;
			$val = str_replace('~', '~0', $val);
			$val = str_replace('/', '~1', $val);
			return $val;
		}, $path);
		return sprintf('/%s', implode('/', $path));
	}

	private function isSaved(): bool
	{
		return !empty($this->data['metadata']['uid']);
	}

	public function __isset($key): bool
	{
		return method_exists($this, $key) || isset($this[$key]);
	}

	public function __get($key): mixed
	{
		if (method_exists($this, $key))
			return $this->{$key}();
		else
			return $this->data[$key];
	}

	public function getIterator(): \Traversable
	{
		return $this->data->getIterator();
	}

	public function toArray(): array
	{
		return $this->data->toArray(true);
	}

	public function offsetExists(mixed $offset): bool
	{
		return isset($this->data[$offset]);
	}

	public function offsetGet(mixed $offset): mixed
	{
		return $this->data[$offset];
	}

	public function offsetSet(mixed $offset, mixed $value): void
	{
		$this->data[$offset] = $value;
	}

	public function offsetUnset(mixed $offset): void
	{
		unset($this->data[$offset]);
	}

	public function kind(): string
	{
		return $this['kind'];
	}

	public function name(): string
	{
		return $this['metadata']['name'];
	}

	public function namespace(): string
	{
		return $this['metadata']['namespace'];
	}

	public function uid(): string
	{
		return $this['metadata']['uid'];
	}

	public function labels(): Attributes\Labels
	{
		if (!isset($this['metadata'])) {
			$this['metadata'] = new \stdClass();
		}
		if (!isset($this['metadata']['labels'])) {
			$this['metadata']['labels'] = new \stdClass();
		}
		return $this->createLabelsAttribute($this['metadata']['labels']);
	}

	public function annotations(): Attributes\Annotations
	{
		if (!isset($this['metadata'])) {
			$this['metadata'] = new \stdClass();
		}
		if (!isset($this['metadata']['annotations'])) {
			$this['metadata']['annotations'] = new \stdClass();
		}
		return $this->createAnnotationsAttribute($this['metadata']['annotations']);
	}

	protected function createLabelsAttribute(Data $labels): Attributes\Labels
	{
		$class = static::$labels[$this->k8s->id()][static::class] ?? static::$labels[static::class] ?? Attributes\Labels::class;
		return new $class(
			data: $labels,
		);
	}

	protected function createAnnotationsAttribute(Data $annotations): Attributes\Annotations
	{
		$class = static::$annotations[$this->k8s->id()][static::class] ?? static::$annotations[static::class] ?? Attributes\Annotations::class;
		return new $class(
			data: $annotations,
		);
	}

	public static function setLabelsAttribute(string $class, ?Kubernetes $k8s = null): void
	{
		if (isset($k8s))
		{
			static::$labels[$k8s->id()] ??= [];
			static::$labels[$k8s->id()][static::class] = $class;
		}
		else
		{
			static::$labels[static::class] = $class;
		}
	}

	public static function setAnnotationsAttribute(string $class, ?Kubernetes $k8s = null): void
	{
		if (isset($k8s))
		{
			static::$annotations[$k8s->id()] ??= [];
			static::$annotations[$k8s->id()][static::class] = $class;
		}
		else
		{
			static::$annotations[static::class] = $class;
		}
	}

}