<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Resources;
use Nucleardog\K8s\Api\ApiVersion;

abstract class ResourceType
{
	public function __construct(
		public readonly string $kind,
		public readonly string $plural,
		public readonly string $singular,
		public readonly array $verbs,
		public readonly array $aliases,
		public readonly ApiVersion $api,
	) {
	}

	public function getCollectionUrl(): string
	{
		return sprintf(
			'%s/%s',
			$this->api->getBaseUrl(),
			$this->plural,
		);
	}

	public function getResourceUrl(string $name): string
	{
		return sprintf(
			'%s/%s/%s',
			$this->api->getBaseUrl(),
			$this->plural,
			$name,
		);
	}

}