<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Resources\Attributes;
use Nucleardog\K8s\Data\Data;

class Attributes extends Data
{
	public function __isset($key): bool
	{
		return method_exists($this, $key) || isset($this[$key]);
	}

	public function __get($key): mixed
	{
		if (method_exists($this, $key))
			return $this->{$key}();
		else
			return $this[$key];
	}

	public function offsetExists(mixed $offset): bool
	{
		return $this->has([(string)$offset]);
	}

	public function offsetGet(mixed $offset): mixed
	{
		return $this->get([(string)$offset]);
	}

	public function offsetSet(mixed $offset, mixed $value): void
	{
		$this->set([(string)$offset], $value);
	}

	public function offsetUnset(mixed $offset): void
	{
		$this->forget([(string)$offset]);
	}
}
