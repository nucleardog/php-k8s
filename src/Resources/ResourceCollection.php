<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Resources;
use Illuminate\Support\Collection;
use Nucleardog\K8s\Kubernetes;
use Nucleardog\K8s\Client\Client;
use Nucleardog\K8s\Exceptions\ResourceNotFoundException;
use Nucleardog\K8s\Watcher\EventType;
use Nucleardog\K8s\Watcher\Watch;
use Nucleardog\K8s\Data\Data;

class ResourceCollection
{
	private readonly Client $client;

	public function __construct(
		private readonly Kubernetes $k8s,
		private readonly ResourceType $type,
		private readonly string $class,
	) {
		$this->client = $k8s->client();
	}

	public function find(string $name): ?Resource
	{
		$response = $this->client->get($this->getResourceUrl($name));

		if ($response->isNotFound())
			return null;

		return $this->hydrate($response->assertKind($this->type->kind)->toArray());
	}

	public function findOrFail(string $name): Resource
	{
		$resource = $this->find($name);

		if (!isset($resource))
			throw new ResourceNotFoundException();

		return $resource;
	}

	public function all(): Collection
	{
		$response = $this->client->get($this->getCollectionUrl())
		                         ->assertListOf($this->type->kind)
		                         ->assertHas('items');
		return $this->hydrateCollection($response['items']->all());
	}

	public function create(array $data = []): Resource
	{
		$data['apiVersion'] = (string)$this->type->api;
		$data['kind'] = $this->type->kind;
		return $this->hydrate($data);
	}

	public function on(EventType $event): Watch
	{
		return $this->k8s->watcher()->on($event, $this->type, $this->class);
	}

	public function whereLabels(Collection $labels): Collection
	{
		$params = [];
		$params['labelSelector'] = implode(
			',',
			$labels->map(fn($value, $key) => $key.'='.$value)->all(),
		);
		$response = $this->client->get($this->getCollectionUrl(), $params)
		                         ->assertListOf($this->type->kind)
		                         ->assertHas('items');
		return $this->hydrateCollection($response['items']->all());
	}

	protected function hydrate(Data|array $data): Resource
	{
		return new $this->class(
			k8s: $this->k8s,
			data: $data,
			type: $this->type,
		);
	}

	protected function hydrateCollection(Data|Collection|array $items): Collection
	{
		$collection = new Collection();
		foreach ($items as $item)
		{
			$collection->push($this->hydrate($item));
		}
		return $collection;
	}

	protected function getCollectionUrl(): string
	{
		return $this->type->getCollectionUrl();
	}

	protected function getResourceUrl(string $name): string
	{
		return $this->type->getResourceUrl($name);
	}
}