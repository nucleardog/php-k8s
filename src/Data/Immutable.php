<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Data;

class Immutable extends Data
{

	public function offsetSet(mixed $offset, mixed $value): void
	{
		throw new \RuntimeException('Data is immutable.');
	}

	public function offsetUnset(mixed $offset): void
	{
		throw new \RuntimeException('Data is immutable.');
	}

	public function __set(string $property, mixed $value): void
	{
		throw new \RuntimeException('Data is immutable.');
	}

	public function __unset(string $property): void
	{
		throw new \RuntimeException('Data is immutable.');
	}

	public function set(string|array $key, mixed $value): void
	{
		throw new \RuntimeException('Data is immutable.');
	}

	public function forget(string|array $key): void
	{
		throw new \RuntimeException('Data is immutable.');
	}

	public function cloneMutable(): Data
	{
		return new Data($this->unwrap());
	}

}