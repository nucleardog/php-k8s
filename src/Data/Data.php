<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Data;

class Data extends \Nucleardog\Data\Data
{
	public function offsetExists(mixed $offset): bool
	{
		return $this->has((string)$offset);
	}

	public function offsetGet(mixed $offset): mixed
	{
		return $this->get((string)$offset);
	}

	public function offsetSet(mixed $offset, mixed $value): void
	{
		$this->set((string)$offset, $value);
	}

	public function offsetUnset(mixed $offset): void
	{
		$this->forget((string)$offset);
	}

}