<?php

declare(strict_types=1);

namespace Nucleardog\K8s;

class KubernetesOptions
{
	public function __construct(
		public readonly string $namespace = 'default',
	) {
	}
}
