<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Client;
use Nucleardog\K8s\Exceptions\KubernetesException;
use Nucleardog\K8s\Exceptions\ResourceNotFoundException;
use Nucleardog\K8s\Data\Immutable as ImmutableData;

class Response extends ImmutableData
{

	public function assertKind(string $kind): static
	{
		if ($this['kind'] !== $kind)
			throw new \RuntimeException(sprintf('Expected kind %s; got %s', $kind, $this['kind']));
		return $this;
	}

	public function assertListOf(string $kind): static
	{
		if ($this['kind'] !== $kind.'List')
			throw new \RuntimeException(sprintf('Expected kind %sList; got %s', $kind, $this['kind']));
		return $this;
	}

	public function assertHas(string $path): static
	{
		if (!$this->offsetExists($path))
			throw new \RuntimeException(sprintf('Response missing field %s', $path));
		return $this;
	}

	public function isSuccessful(): bool
	{
		// TODO: Should whitelist status instead of blacklist failure
		return $this['kind'] !== 'Status' || ($this['kind'] === 'Status' && $this['status'] !== 'Failure');
	}

	public function isNotFound(): bool
	{
		return $this['kind'] === 'Status' &&
		       $this['status'] === 'Failure' &&
		       $this['reason'] === 'NotFound';
	}

	public function throws(): static
	{
		if ($this['kind'] === 'Status' && $this['status'] === 'Failure')
		{
			switch ($this['reason'])
			{
				case 'NotFound':
					throw new ResourceNotFoundException();
				default:
					throw new KubernetesException(sprintf(
						'Request Failed (%s): %s',
						$this['reason'],
						$this['message'],
					));
			}
		}
		return $this;
	}

}