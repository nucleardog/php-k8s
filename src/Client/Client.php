<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Client;
use KubernetesClient\Client as BaseClient;

class Client
{
	public function __construct(
		private readonly BaseClient $client,
	) {
		$client->setDefaultRequestOptions([
			// By default, PHP escapes single quotes within string values when
			// encoding JSON. The only real use case for this is when you want
			// to output JSON in the DOM. The k8s YAML parser, however, is not
			// compatible with the JSON spec and does not accept this.
			'encode_flags' => \JSON_UNESCAPED_SLASHES,
			// Use objects to cleanly differentiate between what should be
			// encoded back to `{}` and `[]`.
			'decode_associate' => false,
		]);
	}

	/**
	 * Make a GET request to the Kubernetes API
	 *
	 * @param string $url
	 * @param array $params
	 * @return Response
	 */
    public function get(string $url, array $params = []): Response
	{
		return $this->request(
			endpoint: $url,
			verb: 'GET',
			params: $params,
		);
	}

	/**
	 * Make a PATCH request to the Kubernetes API
	 *
	 * @param string $url
	 * @param array $body
	 * @param array $params
	 * @return Response
	 */
    public function patch(string $url, array $body = [], array $params = []): Response
	{
		return $this->request(
			endpoint: $url,
			verb: 'PATCH',
			params: $params,
			data: $body,
		);
	}

	/**
	 * Make a PATCH request to the Kubernetes API with a JSON patch
	 *
	 * @param string $url
	 * @param array $body
	 * @param array $params
	 * @return Response
	 */
    public function jsonPatch(string $url, array $body = [], array $params = []): Response
	{
		return $this->request(
			endpoint: $url,
			verb: 'PATCH-JSON',
			params: $params,
			data: $body,
		);
	}

	/**
	 * Make a PATCH request to the Kubernetes API with an apply patch
	 *
	 * @param string $url
	 * @param array $body
	 * @param array $params
	 * @return Response
	 */
    public function apply(string $url, array $body = [], array $params = []): Response
	{
		return $this->request(
			endpoint: $url,
			verb: 'PATCH-APPLY',
			params: $params,
			data: $body,
		);
	}

	/**
	 * Make a POST request to the Kubernetes API
	 *
	 * @param string $url
	 * @param array $body
	 * @param array $params
	 * @return Response
	 */
    public function post(string $url, array $body = [], array $params = []): Response
	{
		return $this->request(
			endpoint: $url,
			verb: 'POST',
			params: $params,
			data: $body,
		);
	}

	/**
	 * Make a PUT request to the Kubernetes API
	 *
	 * @param string $url
	 * @param array $body
	 * @param array $params
	 * @return Response
	 */
    public function put(string $url, array $body = [], array $params = []): Response
	{
		return $this->request(
			endpoint: $url,
			verb: 'PUT',
			params: $params,
			data: $body,
		);
	}

	/**
	 * Make a DELETE request to the Kubernetes API
	 *
	 * @param string $url
	 * @param array $params
	 * @return Response
	 */
    public function delete(string $url, array $params = []): Response
	{
		return $this->request(
			endpoint: $url,
			verb: 'DELETE',
			params: $params,
		);
	}

	/**
	 * Internal request method wrapping the KubernetesClient client
	 * and wrapping the response in our Response object
	 */
	private function request(string $endpoint, string $verb = 'GET', array $params = [], mixed $data = null, array $options = []): Response
	{
		$response = $this->client->request($endpoint, $verb, $params, $data, $options);
		return new Response($response);
	}

}