<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Formats;
use Illuminate\Support\Collection;

class Yaml extends Format
{

	protected function parse(string $contents): Collection
	{
		$this->requireYamlParser();
		$contents = explode(PHP_EOL.'---'.PHP_EOL, $contents);
		$docs = [];
		foreach ($contents as $docContent)
			$docs[] = \Symfony\Component\Yaml\Yaml::parse($docContent);
		return new Collection($docs);
	}

	private function requireYamlParser(): void
	{
		if (!class_exists('Symfony\\Component\\Yaml\\Yaml'))
			throw new \BadFunctionCallException('The symfony/yaml package is not installed.');
	}

}