<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Formats;
use Illuminate\Support\Collection;

class Json extends Format
{

	protected function parse(string $contents): Collection
	{
		// Try and avoid using our splitting implementation except where necessary.
		// Decode the data first and see if it works as-is.
		$data = json_decode($contents, true);

		if ($data !== null)
		{
			// If we were able to parse it, it's a single document and we're done.
			return new Collection([$data]);
		}

		// Try splitting the contents into multiple documents
		$docs = $this->splitJsonDocuments($contents);

		if (sizeof($docs) === 0)
			throw new \RuntimeException('Cannot parse input as JSON.');

		// Parse each document as JSON
		$docs = array_map(fn($content) => json_decode($content, true), $docs);

		// Ensure they all parsed successfully
		foreach ($docs as $doc)
		{
			if (is_null($doc))
				throw new \RuntimeException('Cannot parse input as JSON.');
		}

		return new Collection($docs);
	}

	private function splitJsonDocuments(string $contents): array
	{
		// This is probably a mistake. This tries to extract multiple unseparated JSON objects from a
		// single stream. This is written without any reference to the JSON spec and I already know it
		// is much more tolerant than the spec lays out.
		//
		// If this starts causing a bunch of problems, probably preferable to just drop support for this
		// entirely or go back to only supporting comma-separated objects (which can be handled simply by
		// trying to re-parse a failing document surrounded with `[]` and seeing if that succeeds).
		//
		// As-is, this handles the test document used for kubectl:
		// https://github.com/kubernetes/kubernetes/blob/c9f6d4e1727cc5239e62825b8a2bbb80c353579e/hack/testdata/multi-resource-json.json

		$pairs = [
			'[' => ']',
			'(' => ')',
			'{' => '}',
		];
		$brackets = [];
		$in_escape = false;
		$in_quote = false;

		$documents = [];
		$buffer = '';

		for ($i=0; $i<strlen($contents); $i++) {
			$c = $contents[$i];
			$buffer .= $c;

			if ($in_escape)
			{
				$in_escape = false;
			}
			else if ($in_quote)
			{
				if ($c === '\\')
				{
					$in_escape = true;
				}
				else if ($c === '"')
				{
					$in_quote = false;
				}
			}
			else if (isset($pairs[$c]))
			{
				$brackets[] = $pairs[$c];
			}
			else if (sizeof($brackets) > 0 && $c === end($brackets))
			{
				array_pop($brackets);
				if (sizeof($brackets) === 0)
				{
					if (!empty(trim($buffer)))
						$documents[] = $buffer;
					$buffer = '';
				}
			}
		}

		if (!empty(trim($buffer)))
			$documents[] = $buffer;

		return $documents;
	}

}