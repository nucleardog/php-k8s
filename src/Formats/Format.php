<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Formats;
use Illuminate\Support\Collection;
use Nucleardog\K8s\Kubernetes;

abstract class Format
{
	public function __construct(
		private readonly Kubernetes $k8s,
	) {
	}

	public function fromFile(string $file): Collection
	{
		$contents = file_get_contents($file);
		if ($contents === false)
			throw new \RuntimeException(sprintf('Cannot read file: %s', $file));
		return $this->read($contents);
	}

	public function fromStream($stream): Collection
	{
		$contents = stream_get_contents($stream);
		if ($contents === false)
			throw new \RuntimeException('Cannot read stream.');
		return $this->read($contents);
	}

	public function fromString(string $contents): Collection
	{
		return $this->read($contents);
	}

	protected abstract function parse(string $contents): Collection;

	public function __invoke(string $contents): Collection
	{
		return $this->read($contents);
	}

	public function read(string $contents): Collection
	{
		return $this->parse($contents)->map(fn($doc) => $this->k8s->createResource($doc));
	}

}
