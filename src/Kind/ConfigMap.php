<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Kind;

use Illuminate\Support\Collection;
use Nucleardog\K8s\Resources\Resource;

class ConfigMap extends Resource
{

	public function data(): Collection
	{
		return new Collection($this['data']);
	}

}