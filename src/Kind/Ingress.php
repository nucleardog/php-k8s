<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Kind;

use Illuminate\Support\Collection;
use Nucleardog\K8s\Resources\Resource;

class Ingress extends Resource
{

	public function hosts(): array
	{
		return $this['spec']['rules']->pluck('host')->filter();
	}

	public function services(): Collection
	{
		$services = $this['spec']['rules']
		                ->pluck('http.paths')
		                ->collapse()
		                ->pluck('backend.service.name')
		                ->unique();

		// TODO: Don't load every service to fetch one or two
		return $this->k8s->namespace($this->namespace)->services->all()->whereIn('name', $services);
	}

}