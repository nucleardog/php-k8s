<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Kind;

use Nucleardog\K8s\Resources\Resource;

class Pod extends Resource
{

	public function ip(): string
	{
		return $this['status']['podIP'];
	}

	public function running(): bool
	{
		return $this['status']['phase'] === 'Running';
	}

	public function ready(): bool
	{
		return $this['status']['containerStatuses']->reduce(fn(bool $carry, $status) => $carry && $status['ready'], true);
	}

}