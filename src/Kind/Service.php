<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Kind;

use Illuminate\Support\Collection;
use Nucleardog\K8s\Resources\Resource;
use Nucleardog\K8s\Resources\Attributes\Labels;

class Service extends Resource
{

	public function ip(): string
	{
		return $this['spec']['clusterIP'];
	}

	public function selector(): Collection
	{
		return new Labels(...[
			'k8s' => $this->k8s,
			'data' => $this['spec']['selector']
		]);
	}

	public function pods(): Collection
	{
		return $this->k8s->namespace($this->namespace)->pods->whereLabels($this->selector);
	}

}