<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Kind;

use Illuminate\Support\Collection;
use Nucleardog\K8s\Resources\Resource;
use Nucleardog\K8s\Resources\Attributes\Labels;

class Deployment extends Resource
{

	public function ready(): bool
	{
		return $this['status']['readyReplicas'] === $this['status']['replicas'];
	}

	public function selector(): Collection
	{
		return new Labels($this['spec']['selector']['matchLabels']->all());
	}

	public function pods(): Collection
	{
		return $this->k8s->namespace($this->namespace)->pods->whereLabels($this->selector);
	}

}