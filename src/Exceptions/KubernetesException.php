<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Exceptions;

class KubernetesException extends \Exception
{
}
