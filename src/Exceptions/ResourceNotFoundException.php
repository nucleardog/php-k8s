<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Exceptions;

class ResourceNotFoundException extends KubernetesException
{
}
