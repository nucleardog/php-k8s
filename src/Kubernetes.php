<?php

declare(strict_types=1);

namespace Nucleardog\K8s;

use KubernetesClient\Client as BaseClient;
use Nucleardog\K8s\Client\Client;
use Nucleardog\K8s\Api\Api;
use Nucleardog\K8s\Watcher\Watcher;
use Nucleardog\K8s\Resources\ResourceType;

class Kubernetes
{
	private static int $nextId = 0;
	private readonly int $id;
	private readonly BaseClient $baseClient;
	private readonly Client $client;
	private readonly Api $api;
	private readonly Watcher $watcher;
	private readonly KubernetesOptions $options;

	/** @var array<string,string> */
	private array $kinds;
	/** @var array<string,ResourceType> */
	private array $collections;

	private ?string $namespace = null;
	private ?bool $allNamespaces = null;

	/**
	 * Create a new Kubernetes API client
	 *
	 * @param BaseClient $client
	 * @param ?KubernetesOptions $options
	 */
	public function __construct(BaseClient $client, ?KubernetesOptions $options = null)
	{
		$this->id = static::$nextId++;
		$this->baseClient = $client;
		$this->client = new Client($client);
		$this->api = new Api($this);
		$this->options = $options ?? new KubernetesOptions();
		$this->kinds = [];
		$this->collections = [];
		$this->defaults();
	}

	public function id(): int
	{
		return $this->id;
	}

	private function defaults(): void
	{
		$this->preferCoreApi('pods');
	}

	public function client(): Client
	{
		return $this->client;
	}

	public function api(): Api
	{
		return $this->api;
	}

	public function watcher(): Watcher
	{
		if (empty($this->watcher))
		{
			$this->watcher = new Watcher(
				k8s: $this,
				client: $this->baseClient,
			);
		}

		return $this->watcher;
	}

	public function yaml(): Formats\Yaml
	{
		return new Formats\Yaml(
			k8s: $this,
		);
	}

	public function json(): Formats\Json
	{
		return new Formats\Json(
			k8s: $this,
		);
	}

	public function __get(string $offset)
	{
		$resourceType = $this->getResourceTypeForResource($offset);
		if (empty($resourceType))
			throw new \OutOfBoundsException(sprintf('Unknown api resource type: %s', $offset));

		$namespace = $this->consumeNamespace();
		$allNamespaces = $this->consumeAllNamespaces();

		if ($resourceType instanceof Resources\NamespacedResourceType && !$allNamespaces)
			$resourceType = $resourceType->namespace($namespace);

		return new Resources\ResourceCollection(
			k8s: $this,
			type: $resourceType,
			class: $this->getResourceClassForKind($resourceType->kind),
		);
	}

	public function namespace(string $namespace): static
	{
		$this->namespace = $namespace;
		return $this;
	}

	public function global(): static
	{
		$this->allNamespaces = true;
		return $this;
	}

	private function consumeNamespace(): string
	{
		$namespace = $this->options->namespace;
		if (isset($this->namespace)) {
			$namespace = $this->namespace;
			$this->namespace = null;
		}
		return $namespace;
	}

	private function consumeAllNamespaces(): bool
	{
		$allNamespaces = $this->allNamespaces ?? false;
		$this->allNamespaces = null;
		return $allNamespaces;
	}

	public function registerKind(string $kind, string $class): static
	{
		$this->kinds[$kind] = $class;
		return $this;
	}

	public function preferGroupApi(string $resource, string $group, string $version): static
	{
		$api = $this->api->apis()->where('group', $group)->where('version', $version)->first();
		if (empty($api))
			throw new \OutOfBoundsException(sprintf('Unknown api group: %s/%s', $group, $version));

		$resourceType = $this->api()->resolvePlural($resource, $api);

		if (empty($resourceType))
			throw new \OutOfBoundsException(sprintf('Resource %s not found in api group: %s/%s', $resource, $group, $version));

		$this->collections[$resource] = $resourceType;

		return $this;
	}

	public function preferCoreApi(string $resource): static
	{
		$api = $this->api->apis()['core'];

		if (empty($api))
			throw new \OutOfBoundsException('Could not find core api');

		$resourceType = $this->api()->resolvePlural($resource, $api);

		if (empty($resourceType))
			throw new \OutOfBoundsException(sprintf('Resource %s not found in core api', $resource));

		$this->collections[$resource] = $resourceType;

		return $this;
	}

	public function createResource(array $data): Resources\Resource
	{
		if (empty($data['kind']))
			throw new \RuntimeException('Resource missing "kind".');
		if (empty($data['apiVersion']))
			throw new \RuntimeException('Resource missing "apiVersion".');

		$kind = $data['kind'];
		$apiVersion = $this->api()->resolveApiVersion($data['apiVersion']);

		$resourceType = $this->api()->resolveKind($kind, $apiVersion);
		$class = $this->getResourceClassForKind($kind);

		if (!empty($data['metadata']['namespace']) && $resourceType instanceof Resources\NamespacedResourceType)
			$resourceType = $resourceType->namespace($data['metadata']['namespace']);

		return new $class(
			k8s: $this,
			data: $data,
			type: $resourceType,
		);
	}

	private function getResourceTypeForResource(string $resource): ResourceType
	{
		if (empty($this->collections[$resource]))
		{
			$this->collections[$resource] = $this->api()->resolvePlural($resource);
		}

		if (empty($this->collections[$resource]))
			throw new \OutOfBoundsException(sprintf('Unknown resource type: %s', $resource));

		return $this->collections[$resource];
	}

	private function getResourceClassForKind(string $kind): string
	{
		// If we haven't resolved the class for this kind already (or had
		// one explicitly registered)
		if (empty($this->kinds[$kind]))
		{
			// Try and auto discover from our library namespace

			// Get our class name to use as a base (-> Nucleardog\K8s\Kubernetes)
			$my_class = static::class;
			// Split that up into its parts (-> [ Nucleardog, K8s, Kubernetes ])
			$namespace = explode('\\', $my_class);
			// Remove our class name (-> [ Nucleardog, K8s ])
			array_pop($namespace);
			// Append Kind, $Kind ( -> [ Nucleardog, K8s, Kind, Service])
			$namespace[] = 'Kind';
			$namespace[] = $kind;
			// Implode with a leading slash to start from root (-> \Nucleardog\K8s\Kind\Service)
			$class = '\\'.implode('\\', $namespace);

			// Now see if it exists
			if (class_exists($class, true))
			{
				// If it exists, use that
				$this->kinds[$kind] = $class;
			}
			else
			{
				// Otherwise, fall back to the generic "Resource" class
				$this->kinds[$kind] = Resources\Resource::class;
			}
		}

		return $this->kinds[$kind];
	}

}