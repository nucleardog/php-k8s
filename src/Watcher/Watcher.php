<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Watcher;

use Illuminate\Support\Collection;
use KubernetesClient\Client as BaseClient;
use Nucleardog\K8s\Kubernetes;
use Nucleardog\K8s\Resources\ResourceType;
use Nucleardog\K8s\Data\Data;
use Nucleardog\K8s\Data\Immutable;

class Watcher
{
	private Collection $listeners;
	private ?array $watches;
	private array $buffer;

	public function __construct(
		private BaseClient $client,
		private Kubernetes $k8s,
	) {
		$this->listeners = new Collection();
	}

	public function on(EventType $event, ResourceType $type, string $class): Watch
	{
		// Technically it would work if you only modified existing listeners,
		// so this could be in the if block below. For the sake of not making
		// this method seem unpredictable or confusing (e.g., module A registers
		// while it's not running, module B registers while it is running and it
		// works because the listener already existed, but then removing module A
		// makes module B's listener suddenly fail), we just reject all
		// modifications while we're running.
		// TODO: It would be good to be able to _remove_ a listener while it's
		// running though. We can keep receiving the events and just do nothing
		// with them.
		if ($this->isRunning())
			throw new \RuntimeException('Cannot modify listeners while watches are running.');

		$watch = $this->listeners->where('event', $event)->where('type', $type)->where('class', $class)->first();

		if (empty($watch))
		{
			$watch = new Watch(
				event: $event,
				type: $type,
				class: $class,
			);
			$this->listeners->push($watch);
		}

		return $watch;
	}

	public function isRunning(): bool
	{
		return !empty($this->watches);
	}

	public function watch(int $interval_us = 1000000): \Generator
	{
		for (;;)
		{
			$start = microtime(true);
			$this->internalTick();
			$events = $this->dispatchEvents();
			yield from $events;
			$duration = microtime(true) - $start;
			$duration_us = $duration * 1000000;

			$remaining = (int)($interval_us - $duration_us);
			if ($remaining > 0)
			{
				usleep((int)$remaining);
			}
		}
	}

	public function listen(int $interval_us = 1000000): void
	{
		foreach ($this->watch($interval_us) as $event)
		{
			//
		}
	}

	public function tick(): void
	{
		$this->internalTick();
		$this->dispatchEvents();
	}

	protected function internalTick(): void
	{
		foreach ($this->watches as $watch)
		{
			do
			{
				$lastBufferSize = sizeof($this->buffer);
				$watch->start(1);
			} while (sizeof($this->buffer) > $lastBufferSize);
		}
	}

	protected function dispatchEvents(): array
	{
		$events = [];
		foreach ($this->buffer as $event)
		{
			$events[] = $this->dispatchEvent($event['listener'], $event['event']);
		}
		$this->buffer = [];
		return $events;
	}

	protected function dispatchEvent(Watch $listener, Data $data): array
	{
		$class = $listener->class;
		$resource = new $class(
			k8s: $this->k8s,
			data: $data['object'],
			type: $listener->type,
		);
		$listener($resource);
		return ['listener' => $listener, 'event' => $listener->event, 'resource' => $resource];
	}

	public function start(): static
	{
		if (!empty($this->watches))
			throw new \RuntimeException('Watcher is already running.');

		$this->buffer = [];
		$this->watches = [];

		// TODO: Deduplicate among event types
		$this->watches = $this->listeners->map(function(Watch $listener) {
			// Ignore any with no listeners
			if (count($listener) === 0)
				return null;

			return $this->client->createWatch($listener->type->getCollectionUrl().'?watch=1', [], fn($event) => $event['type'] === $listener->event->value ? ($this->buffer[] = ['listener' => $listener, 'event' => new Immutable($event)]) : null);
		})->filter()->toArray();

		return $this;
	}

	public function stop(): static
	{
		foreach ($this->watches as $watch)
			$watch->resetHandle();
		$this->watches = [];
		return $this;
	}

}