<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Watcher;

enum EventType: string
{
	case ADDED = 'ADDED';
	case MODIFIED = 'MODIFIED';
	case DELETED = 'DELETED';
}