<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Watcher;
use Illuminate\Support\Collection;
use Nucleardog\K8s\Resources\ResourceType;
use Nucleardog\K8s\Resources\Resource;

class Watch implements \Countable
{
	private Collection $callbacks;

	public function __construct(
		public readonly EventType $event,
		public readonly ResourceType $type,
		public readonly string $class,
	) {
		$this->callbacks = new Collection();
	}

	public function count(): int
	{
		return $this->callbacks->count();
	}

	public function call(callable $callback): static
	{
		$this->callbacks->push($callback);
		return $this;
	}

	public function forget(callable $callback): static
	{
		$this->callbacks->reject(fn($cb) => $cb === $callback);
		return $this;
	}

	public function __invoke(Resource $resource): void
	{
		$this->callbacks->each(fn($callback) => $callback($this->event, $resource, $this));
	}

}