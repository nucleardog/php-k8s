<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Api;
use Illuminate\Support\Collection;
use Nucleardog\K8s\Kubernetes;
use Nucleardog\K8s\Client\Client;

abstract class ApiVersion
{
	private readonly Client $client;

	public function __construct(
		private readonly Kubernetes $k8s,
		private readonly ?string $group,
		private readonly string $version,
	) {
		$this->client = $k8s->client();
	}

	public abstract function getBaseUrl(): string;
	public abstract function __toString(): string;

	public function getResources(): Collection
	{
		$response = $this->client->get($this->getBaseUrl())
		                       ->assertKind('APIResourceList')
		                       ->assertHas('resources')
		                       ->toArray(true);

		return collect($response['resources'])->mapWithKeys(function($resource) {

			$class = $resource['namespaced'] ?
					\Nucleardog\K8s\Resources\NamespacedResourceType::class :
					\Nucleardog\K8s\Resources\GlobalResourceType::class;

			return [
				$resource['name'] => new $class(
					kind: $resource['kind'],
					plural: $resource['name'],
					singular: $resource['singularName'],
					//namespaced: $resource['namespaced'],
					verbs: $resource['verbs'] ?? [],
					aliases: $resource['shortNames'] ?? [],
					api: $this,
				),
			];

		});
	}

	public function equals(ApiVersion $apiVersion): bool
	{
		return $this->group === $apiVersion->group &&
		       $this->version === $apiVersion->version;
	}

}