<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Api;
use Nucleardog\K8s\Kubernetes;

class GroupApi extends ApiVersion
{
	public function __construct(
		private readonly Kubernetes $k8s,
		public readonly string $group,
		public readonly string $version,
	) {
		parent::__construct(
			k8s: $k8s,
			group: $group,
			version: $version,
		);
	}

	public function getBaseUrl(): string
	{
		return sprintf('/apis/%s/%s', $this->group, $this->version);
	}

	public function __toString(): string
	{
		return sprintf('%s/%s', $this->group, $this->version);
	}

}