<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Api;
use Nucleardog\K8s\Kubernetes;

class CoreApi extends ApiVersion
{

	public function __construct(
		private Kubernetes $k8s,
		public readonly string $version,
	) {
		parent::__construct(
			k8s: $k8s,
			group: null,
			version: $version,
		);
	}

	public function getBaseUrl(): string
	{
		return sprintf('/api/%s', $this->version);
	}

	public function __toString(): string
	{
		return $this->version;
	}

}