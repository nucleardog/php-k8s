<?php

declare(strict_types=1);

namespace Nucleardog\K8s\Api;
use Illuminate\Support\Collection;
use Nucleardog\K8s\Kubernetes;
use Nucleardog\K8s\Client\Client;
use Nucleardog\K8s\Resources\ResourceType;

class Api
{
	private const CORE_API_VERSION = 'v1';

	private readonly Client $client;
	private Collection $apis;
	private Collection $resources;

	public function __construct(
		private readonly Kubernetes $k8s
	) {
		$this->client = $k8s->client();
		$this->refresh();
	}

	public function refresh(): static
	{
		$this->apis = $this->getApiGroups()
		                   ->merge(['core' => $this->getCoreApi()]);

		$this->resources = new Collection();
		$this->apis->each(function($api) {
			$this->resources = $this->resources->merge($api->getResources()->values());
		});

		return $this;
	}

	protected function getCoreApi(): ApiVersion
	{
		return new CoreApi(
			k8s: $this->k8s,
			version: static::CORE_API_VERSION,
		);
	}

	protected function getApiGroups(): Collection
	{
		$response = $this->client->get('/apis')
		                         ->assertListOf('APIGroup')
		                         ->assertHas('groups')
		                         ->toArray(true);

		return collect($response['groups'])->mapWithKeys(function($group) {
			return [
				$group['name'] => new GroupApi(
					k8s: $this->k8s,
					group: $group['name'],
					version: $group['preferredVersion']['version'],
				),
			];
		});
	}

	public function apis(): Collection
	{
		return $this->apis;
	}

	public function resources(): Collection
	{
		return $this->resources;
	}

	public function resolveApiVersion(string $apiVersion): ApiVersion
	{
		$offset = strrpos($apiVersion, '/');
		if ($offset !== false)
		{
			$group = substr($apiVersion, 0, $offset);
			$version = substr($apiVersion, $offset+1);
			$api = $this->apis()->where('group', $group)->where('version', $version)->first();
		}
		else
		{
			$api = $this->apis()['core'];
		}

		if (empty($api))
			throw new \OutOfBoundsException(sprintf('apiVersion %s not found', $apiVersion));

		return $api;
	}

	public function resolveKind(string $kind, ?ApiVersion $apiVersion = null): ResourceType
	{
		$matches = $this->resources()->where('kind', $kind);
		if (!empty($apiVersion))
		{
			$matches = $matches->filter(function($resourceType) use ($apiVersion) {
				return $resourceType->api->equals($apiVersion);
			});
		}
		if ($matches->count() === 0)
			throw new \OutOfBoundsException(sprintf('Kind %s not found', $kind));
		return $matches->first();
	}

	public function resolvePlural(string $plural, ?ApiVersion $apiVersion = null): ResourceType
	{
		$matches = $this->resources()->where('plural', $plural);
		if (!empty($apiVersion))
		{
			$matches = $matches->filter(function($resourceType) use ($apiVersion) {
				return $resourceType->api->equals($apiVersion);
			});
		}
		if ($matches->count() === 0)
			throw new \OutOfBoundsException(sprintf('Resource plural %s not found', $plural));
		return $matches->first();
	}

}