<?php
/**
 * This is an automatically generated baseline for Phan issues.
 * When Phan is invoked with --load-baseline=path/to/baseline.php,
 * The pre-existing issues listed in this file won't be emitted.
 *
 * This file can be updated by invoking Phan with --save-baseline=path/to/baseline.php
 * (can be combined with --load-baseline)
 */
return [
    // # Issue statistics:
    // PhanUndeclaredFunctionInCallable : 9 occurrences
    // PhanImpossibleCondition : 8 occurrences
    // PhanClassContainsAbstractMethod : 6 occurrences
    // PhanTypeMismatchReturn : 4 occurrences
    // PhanUndeclaredProperty : 4 occurrences
    // PhanTypeMismatchArgument : 3 occurrences
    // PhanCoalescingNeverNull : 2 occurrences
    // PhanPartialTypeMismatchArgument : 2 occurrences
    // PhanTypeMismatchArgumentReal : 2 occurrences
    // PhanAccessReadOnlyProperty : 1 occurrence
    // PhanInfiniteLoop : 1 occurrence
    // PhanParamTooMany : 1 occurrence
    // PhanPartialTypeMismatchReturn : 1 occurrence
    // PhanPluginDuplicateConditionalNullCoalescing : 1 occurrence
    // PhanPluginEmptyStatementForeachLoop : 1 occurrence
    // PhanRedundantCondition : 1 occurrence
    // PhanRedundantConditionInLoop : 1 occurrence
    // PhanTypeObjectUnsetDeclaredProperty : 1 occurrence
    // PhanUndeclaredClassMethod : 1 occurrence
    // PhanUnusedVariable : 1 occurrence

    // Currently, file_suppressions and directory_suppressions are the only supported suppressions
    'file_suppressions' => [
        'src/Api/Api.php' => ['PhanTypeMismatchReturn', 'PhanUndeclaredFunctionInCallable'],
        'src/Api/ApiVersion.php' => ['PhanUndeclaredProperty'],
        'src/Client/Client.php' => ['PhanPartialTypeMismatchArgument'],
        'src/Formats/Yaml.php' => ['PhanUndeclaredClassMethod'],
        'src/Kind/Service.php' => ['PhanParamTooMany', 'PhanPartialTypeMismatchArgument'],
        'src/Kubernetes.php' => ['PhanAccessReadOnlyProperty', 'PhanImpossibleCondition', 'PhanRedundantCondition', 'PhanTypeMismatchArgument', 'PhanTypeObjectUnsetDeclaredProperty', 'PhanUndeclaredFunctionInCallable'],
        'src/RecursiveCollection.php' => ['PhanClassContainsAbstractMethod', 'PhanImpossibleCondition', 'PhanPluginDuplicateConditionalNullCoalescing', 'PhanTypeMismatchReturn'],
        'src/Resources/Attributes/Annotations.php' => ['PhanClassContainsAbstractMethod'],
        'src/Resources/Attributes/Labels.php' => ['PhanClassContainsAbstractMethod'],
        'src/Resources/NamespacedResourceType.php' => ['PhanCoalescingNeverNull'],
        'src/Resources/ResourceCollection.php' => ['PhanTypeMismatchArgumentReal'],
        'src/Watcher/Watch.php' => ['PhanTypeMismatchArgument'],
        'src/Watcher/Watcher.php' => ['PhanInfiniteLoop', 'PhanPartialTypeMismatchReturn', 'PhanPluginEmptyStatementForeachLoop', 'PhanRedundantConditionInLoop', 'PhanTypeMismatchArgumentReal', 'PhanUndeclaredFunctionInCallable', 'PhanUnusedVariable'],
    ],
    // 'directory_suppressions' => ['src/directory_name' => ['PhanIssueName1', 'PhanIssueName2']] can be manually added if needed.
    // (directory_suppressions will currently be ignored by subsequent calls to --save-baseline, but may be preserved in future Phan releases)
];
